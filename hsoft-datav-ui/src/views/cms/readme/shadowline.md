# 阴影折线图

## 一、配置

- 图层名称：修改组件图层的名称。建议设置，该内容和左侧控制面板该图表插件名称一致，便于后期图表多时管理；
                                                   
- 标题：标题显示的内容，如果不想显示标题，不填写内容就不显示；
          
- 副标题：副标题内容，如果不想显示副标题，不填写内容就不显示；

- x轴名称：x轴坐标轴名称；
                
- 显示图例开关：该开关控制图例的显示与隐藏，不显示图例情况如下图；
  <viewer>
  <img src="../image/shadowline-1.png" width="60%">
  </viewer>

- 显示x轴开关：该开关控制是否显示x轴；

- y轴数字：该开关控制是否显示y轴数字与其对应的分割线，关闭效果如下图；
  <viewer>
  <img src="../image/shadowline-2.png" width="60%">
  </viewer>
              
- 是否平滑开关：图表中的曲线是否平滑显示，关闭则表示不开启平滑处理，如下图；
  <viewer>
  <img src="../image/shadowline-3.png" width="60%">
  </viewer>

- 显示平均线开关：是否显示图表折线的平均值，颜色和折线一致，如下图；
  <viewer>
  <img src="../image/shadowline-4.png" width="60%">
  </viewer>

- 显示最值开关：是否显示图表折线的最值（最大值和最小值），如下图；
  <viewer>
  <img src="../image/shadowline-5.png" width="60%">
  </viewer>

- 分隔线开关：是否显示y轴的分隔线，关闭则不显示分隔线，如下图；
  <viewer>
  <img src="../image/shadowline-6.png" width="60%">
  </viewer>

- 分隔线样式：分隔线样式分为「实线」「虚线」「点线」；
  <viewer>
  <img src="../image/shadowline-7-1.png" width="30%">
  <img src="../image/shadowline-7-2.png" width="30%">
  <img src="../image/shadowline-7-3.png" width="30%">
  </viewer>

- 载入动画：弹跳、渐隐渐现、向右滑入、向左滑入、放缩、旋转、滚动；

## 二、数据

静态数据格式：

```
 （1）一组折线图静态数据格式：
    [
      {
        "name":"line1",
        "data":[18.7,48.3,103.5,231.6,46.6,55.4,18.4],
        "axisData":["Mon","Tue","Wed","Thu","Fri","Sat","Sun"]
      }
    ]

 （2）多组折线图静态数据格式：
    [
      {
        "name":"line1",
        "data":[7000,6900,9500,14500,18400,21500,25200,26500,23300,18300,13900,9600],
        "axisData":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"]
      },
      {
        "name":"line2",
        "data":[3000,4200,5700,8500,11900,15200,17000,16600,14200,10300,6600,4800],
        "axisData":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"]
      },
      {
        "name":"line3",
        "data":[3000,4800,17000,1500,11900,19000,16000,27900,15200,10000,8600,7800],
        "axisData":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"]
      }
    ]
```
