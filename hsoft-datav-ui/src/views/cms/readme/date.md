# 实时时间

## 一、配置

- 图层名称：组件的名称。（建议设置，方便后期组件管理）

- 字体大小：文本字体大小，可以调节改变，例如设置字体大小为 50，如下图；

- 字体名称：文本字体名称，可设置为宋体、黑体、微软雅黑、Digital、Chunkfive、unidreamLED、
  Arial、Helvetica、sans-serif，例如设置字体名称为“黑体”，如下图；

- 字体颜色：文本字体颜色，例如设置字体颜色为白色，如下图；

  <viewer>
  <img src="../image/date-1.png"width="60%">
  </viewer>

- 字体间距：文本字体间距，例如设置字体间距为 10，如下图；

  <viewer>
  <img src="../image/date-2.png"width="60%">
  </viewer>

- 字体背景：文本字体背景颜色，例如设置字体背景为黄色，如下图；

  <viewer>
  <img src="../image/date-3.png"width="60%">
  </viewer>

- 文字粗细：文本字体粗细，可设置为 normal、bold、bolder、lighter，例如设置为“bold”（加粗），如下图；

  <viewer>
  <img src="../image/date-4.png"width="60%">
  </viewer>

- 对齐方式：文本在组件中的位置，可设置为居左、居中、居右、例如设置为居左，如下图；

  <viewer>
  <img src="../image/date-5.png"width="60%">
  </viewer>

- 时间格式：设置时间的显示格式，可为日期、日期+时分、日期+时分秒、日期（无年）、时分、时分秒、星期，分别显示如下图；

  <viewer>
  <img src="../image/date-7.png"width="60%">
  </viewer>

- 是否自定义格式开关：控制是否按照用户输入自定义日期格式显示，其中用 yyyy 代表年份，MM 代表月份，dd 代表日，
  HH 代表 24 小时制小时数，hh 代表 12 小时制小时数，mm 代表分钟，ss 代表秒数，SSS 代表毫秒，day 代表星期，可自由组合，
  例如输入 yyyy 年 MM 月 dd 日 day,显示结果如下图；

  <viewer>
  <img src="../image/date-6.png"width="60%">
  </viewer>

- 载入动画：组件的载入动画效果。分别为：弹跳、渐隐渐显、向右滑入、向左滑入、放缩、旋转、滚动。
