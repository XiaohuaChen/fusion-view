# 输入框

## 配置

- 图层名称：修改组件图层的名称。建议设置，该内容和左侧控制面板该图表插件名称一致，便于后期图表多时管理；
- label 内容/字号/字体/颜色：设置 label 有关项；
  <viewer>
  <img src="../image/input-1.png" width="50%">
  </viewer>

- input 的 name：用于设置 input 的 name 属性，与后端参数的字段相对应，可根据该属性进行过滤查询；

- input 的最大长度：input 的最大输入长度；

- 宽度：input 输入框的宽度；

- input 输入类型：
  <viewer>
  <img src="../image/input-2.png" width="50%">
  </viewer>

- 绑定组件：绑定所要联动的组件；
  <viewer>
  <img src="../image/input-3.png" width="40%">  
   <img src="../image/input-4.png" width="40%">
  </viewer>

- 载入动画：弹跳、渐隐渐现、向右滑入、向左滑入、放缩、旋转、滚动；
