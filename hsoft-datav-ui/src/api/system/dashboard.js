import request from '@/utils/request'

/** 查找已注册用户人数 **/
export function findRegister() {
  return request({
    url: '/system/dashboard/findRegister',
    method: 'post',
  })
}

/** 查找在线用户人数 **/
export function findOnlineUsers() {
  return request({
    url: '/system/dashboard/findOnlineUsers',
    method: 'post',
  })
}

/** 查询大屏模板个数 **/
export function findScreenCount() {
  return request({
    url: '/system/dashboard/findScreenCount',
    method: 'post',
  })
}

/** 查询组件个数 **/
export function findAssemblyCount() {
  return request({
    url: '/system/dashboard/findAssemblyCount',
    method: 'post',
  })
}

/** 近一周每日新增用户数变化趋势 **/
export function findFirstList(data) {
  return request({
    url: '/system/dashboard/findFirstList',
    method: 'post',
    data: data
  })
}

/** 近一周大屏模板个数变化趋势 **/
export function findThirdList(data) {
  return request({
    url: '/system/dashboard/findThirdList',
    method: 'post',
    data: data

  })
}

/** 近一周组件个数变化趋势 **/
export function findForthList(data) {
  return request({
    url: '/system/dashboard/findForthList',
    method: 'post',
    data: data
  })
}

/** 查询是否公开的集合信息 **/
export function findIsPublic() {
  return request({
    url: '/system/dashboard/findIsPublic',
    method: 'post',
  })
}

/** 查询是否是否是模板的集合信息 **/
export function findIsTemplate() {
  return request({
    url: '/system/dashboard/findIsTemplate',
    method: 'post',
  })
}

/** 查询自定义组件数量变化 **/
export function findCustom() {
  return request({
    url: '/system/dashboard/findCustom',
    method: 'post',
  })
}
/** 查询自定义组件数量变化 **/
export function findCustomPie() {
  return request({
    url: '/system/dashboard/findCustomPie',
    method: 'post',
  })
}