import request from '@/utils/request'

// 数据源信息列表
export function listSourse(query) {
    return request({
        url: '/datav/datasourse/list',
        method: 'get',
        params: query
    })
}

// 查询数据源详细
export function getSourse(id) {
    return request({
        url: '/datav/datasourse/' + id,
        method: 'get'
    })
}

// 新增数据源
export function addSourse(data) {
    return request({
        url: '/datav/datasourse',
        method: 'post',
        data: data
    })
}

// 修改数据源信息
export function updateSourse(data) {
    return request({
        url: '/datav/datasourse',
        method: 'put',
        data: data
    })
}

// 删除数据源信息
export function delSourse(ids) {
    return request({
        url: '/datav/datasourse/' + ids,
        method: 'delete'
    })
}

