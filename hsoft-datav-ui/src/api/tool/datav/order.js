import request from '@/utils/request'

// 工单信息列表
export function listWorkorder(query) {
    return request({
        url: '/datav/workorder/list',
        method: 'get',
        params: query
    })
}

// 查询工单详细
export function getWorkorder(id) {
    return request({
        url: '/datav/workorder/' + id,
        method: 'get'
    })
}

// 新增工单
export function addWorkorder(data) {
    return request({
        url: '/datav/workorder',
        method: 'post',
        data: data
    })
}

// 修改工单信息
export function updateWorkorder(data) {
    return request({
        url: '/datav/workorder',
        method: 'put',
        data: data
    })
}

// 删除工单信息
export function delWorkorder(ids) {
    return request({
        url: '/datav/workorder/' + ids,
        method: 'delete'
    })
}

// 受理工单信息
export function acceptWorkorder(data) {
    return request({
        url: '/datav/workorder/accept',
        method: 'post',
        data: data
    })
}

// 解决工单信息
export function completeWorkorder(data) {
    return request({
        url: '/datav/workorder/complete',
        method: 'post',
        data: data
    })
}

// 删除工单附件
export function delFile(url) {
    return request({
        url: '/datav/workorder/delFile',
        method: 'post',
        data: url
    })
}
