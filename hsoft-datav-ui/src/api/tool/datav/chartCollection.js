import request from '@/utils/request'

// 查询组件收藏库列表
export function listChartCollection(query) {
  return request({
    url: '/datav/chartcollection/list',
    method: 'get',
    params: query
  })
}

// 查询组件收藏详细
export function getChartCollection(id) {
  return request({
    url: '/datav/chartcollection/' + id,
    method: 'get'
  })
}

// 新增组件收藏
export function addChartCollection(data) {
  return request({
    url: '/datav/chartcollection',
    method: 'post',
    data: data
  })
}

// 删除自定义组件库
export function delCollectchart(collectionId) {
  return request({
    url: '/datav/chartcollection/' + collectionId,
    method: 'delete'
  })
}

